package com.webcorp.ca.Helper;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


	public class Utility {
		static WebDriver driver;
			
			public static  WebDriver openBrowser(){
				
				driver=new FirefoxDriver();
				
				driver.manage().window().maximize();
				
				driver.manage().deleteAllCookies();
				
				driver.get("https://beatties-test.basics.com");
				
				return driver;
				
			}

			
			
		

}
