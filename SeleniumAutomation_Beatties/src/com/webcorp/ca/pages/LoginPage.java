package com.webcorp.ca.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {
	WebDriver driver;
    By SignInButton=By.xpath(".//*[@id='userRegisterButton']");
	
	By logonId=By.xpath(".//*[@id='logonId']");
	
	By password=By.xpath(".//*[@id='password']");
	
	By submit=By.xpath(".//*[@id='loginForm']/div[5]/a[1]");
	
	
	
	public LoginPage(WebDriver driver)
	{
		this.driver=driver;
		
    }
	
	public void SignIn(){
		
		driver.findElement(SignInButton).click();
		driver.findElement(logonId).sendKeys("w8t001u1@webcorpinc.ca");
		driver.findElement(password).sendKeys("00-18596");
		driver.findElement(submit).click();
		
	}

}
