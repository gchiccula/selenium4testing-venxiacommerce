package com.webcorp.ca.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class ShippingAddressPage {
	
WebDriver driver;
	
	By firstName=By.xpath(".//*[@id='firstName']");
	
	By lastName=By.xpath(".//*[@id='lastName']");
	
	By companyName=By.xpath(".//*[@id='company_name']");
	
	By address1=By.xpath(".//*[@id='address1']");
	
	By address2=By.xpath(".//*[@id='address2']");
	
	By city=By.xpath(".//*[@id='city']");
	
	By state=By.xpath(".//*[@id='province']");
	
	By zipCode=By.xpath(".//*[@id='postalcode']");
	
	By Country=By.xpath(".//*[@id='country']");
	
	By phoneNumber=By.xpath(".//*[@id='cellphone']");
	
	By email1=By.xpath(".//*[@id='emailaddress']");
	
	By ConfirmEmail1=By.xpath(".//*[@id='confirm_emailaddress']");
	
	
	
public ShippingAddressPage(WebDriver driver)
	{
		this.driver=driver;
		
    }
	
	public void FillShippingAddressForm(){
		
		driver.findElement(firstName).sendKeys("venkata");
		driver.findElement(lastName).sendKeys("chakka");
		driver.findElement(address1).sendKeys("toronto");
		driver.findElement(zipCode).sendKeys("A1A 1A1");
		driver.findElement(city).sendKeys("WATERLOO");
		driver.findElement(state).click();
		new Select(driver.findElement(state)).selectByVisibleText("	British Columbia");
		driver.findElement(Country).click();
		new Select(driver.findElement(Country)).selectByVisibleText("Canada");
		driver.findElement(phoneNumber).sendKeys("9059536043");
		/*driver.findElement(email1).sendKeys("gchiccula@webcorpinc.ca");
		driver.findElement(ConfirmEmail1).sendKeys("gchiccula@webcorpinc.ca");*/
	}

}
