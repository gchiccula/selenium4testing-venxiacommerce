package com.webcorp.ca.TestCases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.webcorp.ca.Helper.Utility;
import com.webcorp.ca.pages.BillingAddressPage;
import com.webcorp.ca.pages.LoginPage;

public class Test10 {
	
public WebDriver driver;
	
	@Test (description="verify the functionality of  submit order with register user")
	public void AlertPopUpForShippingCharges() throws Exception{
		Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='autocomplete']")).sendKeys("pen");
		
		driver.findElement(By.xpath(".//*[@id='wci_searchBox']/div/div/span/a/img")).click();
		
		driver.findElement(By.xpath(".//*[@id='addtoCart_43037']/a/span/img")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='addtoCart_55902']/a/span/img")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='widget_minishopcart']")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='GotoCartButton1']")).click();
		Thread.sleep(5000);
		//driver.findElement(By.xpath(".//*[@id='field3']")).click();
		driver.findElement(By.xpath(".//*[@id='field3']")).sendKeys("4444");
        Thread.sleep(3000);
		driver.findElement(By.xpath(".//*[@id='submitorderbutton']")).click();
		try {
			driver.switchTo().alert().accept();
			
		} catch (Exception e) {
			System.out.println("shipping charges are not added");
		}
		Thread.sleep(8000);
	}
	
	@BeforeClass
	public void OpenBrowser() throws Exception{
	
		driver=Utility.openBrowser();
			Thread.sleep(5000);
		
		
		}

@BeforeClass
public void SignIn() throws Exception{
	
	LoginPage login=PageFactory.initElements(driver, LoginPage.class);
	
	login.SignIn();
	Thread.sleep(5000);
}
@AfterClass
public void LogOut() throws Exception{
	driver.findElement(By.xpath(".//*[@id='myaccountButton']")).click();
	Thread.sleep(3000);
	driver.findElement(By.xpath(".//*[@id='logOut']")).click();
	Thread.sleep(3000);
	driver.quit();
	
}

}
