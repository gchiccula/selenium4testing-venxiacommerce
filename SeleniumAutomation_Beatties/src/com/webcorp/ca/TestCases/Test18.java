package com.webcorp.ca.TestCases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.webcorp.ca.Helper.Utility;

public class Test18 {
	
public WebDriver driver;
	
	@Test(description="Verify The functionality of AddToCart In QuickOrderForm")
	 public void QuickOrderFormAddToCart() throws Exception{
		driver.findElement(By.xpath(".//*[@id='myAccountQuickLink']")).click();
		
		driver.findElement(By.xpath(".//*[@id='itemNumber_1']")).sendKeys("44303-00");
		
		Thread.sleep(4000);
		driver.findElement(By.xpath(".//*[@id='itemNumber_2']")).sendKeys("");
		Thread.sleep(4000);
		
		try {
			driver.findElement(By.xpath(".//*[@id='itemNumber_2']")).sendKeys("38214-00");
			Thread.sleep(4000);
			driver.findElement(By.xpath(".//*[@id='itemNumber_3']")).sendKeys("");
			Thread.sleep(4000);
			driver.findElement(By.xpath(".//*[@id='addtoCart']/img")).click();
			
		} catch (Exception e) {
			driver.findElement(By.xpath(".//*[@id='itemNumber_2']")).clear();
			Thread.sleep(4000);
			driver.findElement(By.xpath(".//*[@id='addtoCart']/img")).click();
		}
		Thread.sleep(8000);
		driver.findElement(By.xpath(".//*[@id='widget_minishopcart']")).click();
		Thread.sleep(4000);
		//driver.findElement(By.xpath(".//*[@id='widget_minishopcart']")).click();
		Thread.sleep(4000);
		driver.findElement(By.xpath(".//*[@id='widget_minishopcart']")).click();
		Thread.sleep(4000);
		driver.findElement(By.xpath(".//*[@id='contentImage_1_HeaderStoreLogo_Content']")).click();
		
		 
	 }
	
	
	
	@BeforeClass
	public void OpenBrowser(){
		driver=Utility.openBrowser();
		
	}
	@AfterClass
	public void CloseBrowser(){
		
		driver.quit();
	}


}
