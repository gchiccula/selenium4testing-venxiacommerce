package com.webcorp.ca.TestCases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.webcorp.ca.Helper.Utility;

public class Test08 {
	
	
public WebDriver driver;
	
	@Test(description="Verify The Functionality of SignIn")
	 public void Login() throws Exception{
		driver.findElement(By.xpath(".//*[@id='userRegisterButton']")).click();
		
		driver.findElement(By.xpath(".//*[@id='logonId']")).sendKeys("w8t001u1@webcorpinc.ca");
		
		driver.findElement(By.xpath(".//*[@id='password']")).sendKeys("00-18596");
		
		driver.findElement(By.xpath(".//*[@id='loginForm']/div[5]/a[1]")).click();
		
	 }
	
	
	
	@BeforeClass
	public void OpenBrowser(){
		driver=Utility.openBrowser();
		
	}
	@AfterClass
	public void Logout() throws Exception{
		
		
		driver.quit();
	}

}
