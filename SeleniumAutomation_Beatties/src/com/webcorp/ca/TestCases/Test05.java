package com.webcorp.ca.TestCases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.webcorp.ca.Helper.Utility;

public class Test05 {
	
public WebDriver driver;
	
	@Test(description="Click All Banner Ads home Page")
	public void ClickHomeDealerBannerAd(){
		driver.findElement(By.xpath(".//*[@id='homeDealerBannerAd']/li[3]/a/img")).click();
		driver.findElement(By.xpath(".//*[@id='contentImage_1_HeaderStoreLogo_Content']")).click();
		driver.findElement(By.xpath("html/body/div[2]/div/div/div/div[1]/div/div/div[2]/div[1]/div[3]/a")).click();
		driver.findElement(By.xpath(".//*[@id='homeDealerBannerAd']/li[4]/a/img")).click();
		driver.findElement(By.xpath(".//*[@id='contentImage_1_HeaderStoreLogo_Content']")).click();
		driver.findElement(By.xpath("html/body/div[2]/div/div/div/div[1]/div/div/div[2]/div[1]/div[4]/a")).click();
		driver.findElement(By.xpath(".//*[@id='homeDealerBannerAd']/li[5]/a/img")).click();
		driver.findElement(By.xpath(".//*[@id='contentImage_1_HeaderStoreLogo_Content']")).click();
		driver.findElement(By.xpath("html/body/div[2]/div/div/div/div[1]/div/div/div[2]/div[1]/div[1]/a")).click();
		driver.findElement(By.xpath(".//*[@id='homeDealerBannerAd']/li[2]/a/img")).click();
		driver.findElement(By.xpath(".//*[@id='contentImage_1_HeaderStoreLogo_Content']")).click();
		/*driver.findElement(By.xpath("")).click();
		driver.findElement(By.xpath("")).click();
		driver.findElement(By.xpath("")).click();
		driver.findElement(By.xpath("")).click();*/
	}
	
	
	@BeforeClass
	public void OpenBrowser(){
		driver=Utility.openBrowser();
		
	}
	@AfterClass
	public void CloseBrowser(){
		
		driver.quit();
	}


}
