package com.webcorp.ca.TestCases;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.webcorp.ca.Helper.Utility;
import com.webcorp.ca.pages.BillingAddressPage;
import com.webcorp.ca.pages.PaymentPage;

public class Test06 {
	
public WebDriver driver;
	
	@Test(description="Verify The Functionality Of shopping cart when we are scroll down the page")
	 public void widgetMiniShopCart() throws Exception{
		driver.findElement(By.xpath(".//*[@id='homePageNationalAds']/div[3]/a/img")).click();
		
		Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='ProductListDataTableId_38269-00']/td[5]/span/img")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='contentImage_1_HeaderStoreLogo_Content']")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='homePageNationalAds']/div[4]/a/img")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='ProductListDataTableId_74053-00']/td[5]/span/img")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='contentImage_1_HeaderStoreLogo_Content']")).click();
		Thread.sleep(5000);
		JavascriptExecutor jse = (JavascriptExecutor)driver; 
		jse.executeScript("window.scrollBy(0,1000)", "");
		
		/*driver.findElement(By.xpath(".//*[@id='homePageNationalAds']/div[1]/a/img")).click();
		driver.findElement(By.xpath(".//*[@id='homePageNationalAds']/div[1]/a/img")).click();
		driver.findElement(By.xpath(".//*[@id='homePageNationalAds']/div[1]/a/img")).click();
		driver.findElement(By.xpath(".//*[@id='homePageNationalAds']/div[1]/a/img")).click();*/
		
		driver.findElement(By.xpath(".//*[@id='widget_minishopcart']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath(".//*[@id='widget_minishopcart']")).click();
		driver.findElement(By.xpath(".//*[@id='GotoCartButton1']/span")).click();
		Thread.sleep(4000);
		driver.findElement(By.xpath(".//*[@id='submitorderbutton']")).click();
		Thread.sleep(4000);
		 BillingAddressPage billing=PageFactory.initElements(driver, BillingAddressPage.class);
			
			billing.FillBillingAddressForm();
		
		driver.findElement(By.xpath(".//*[@id='WC_UserRegistrationAddForm_links_1']/div[2]")).click();
		Thread.sleep(4000);
		driver.findElement(By.xpath(".//*[@id='sameAsBilling']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath(".//*[@id='WC_UserRegistrationAddForm_links_1']/div[2]")).click();
		Thread.sleep(4000);
		PaymentPage payment=PageFactory.initElements(driver, PaymentPage.class);
		payment.paymentPageForm();
		Thread.sleep(5000);
		
		driver.findElement(By.xpath(".//*[@id='submitorderbutton']")).click();
		try {
			driver.switchTo().alert().accept();
			
		} catch (Exception e) {
			System.out.println("shipping charges are not added");
		}
		Thread.sleep(10000);
		driver.findElement(By.xpath("html/body/div[2]/div/div[1]/div/div[2]/div[2]/p[4]/span/a/button")).click();
		 
	 }
	
	
	
	@BeforeClass
	public void OpenBrowser(){
		driver=Utility.openBrowser();
		
	}
	@AfterClass
	public void CloseBrowser(){
		
		driver.quit();
	}

	
	

}
