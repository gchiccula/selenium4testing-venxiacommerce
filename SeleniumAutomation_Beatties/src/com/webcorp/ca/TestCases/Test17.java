package com.webcorp.ca.TestCases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.webcorp.ca.Helper.Utility;

public class Test17 {
	
public WebDriver driver;
	
	@Test(description="Verify The functionality of SortBy in SearchResultspage")
	 public void SortingSearchResults() throws Exception{
		driver.findElement(By.xpath(".//*[@id='autocomplete']")).click();
		
		driver.findElement(By.xpath(".//*[@id='autocomplete']")).sendKeys("paper");
		
		Thread.sleep(4000);
		
		driver.findElement(By.xpath(".//*[@id='wci_searchBox']/div/div/span/a/img")).click();
		
		driver.findElement(By.xpath(".//*[@id='wci_searchPage_controls_desktop']/div/div[1]/div[1]/select")).click();
		
		driver.findElement(By.xpath(".//*[@id='wci_searchPage_controls_desktop']/div/div[1]/div[1]/select/option[6]")).click();
		
		driver.findElement(By.xpath(".//*[@id='wci_searchPage_controls_desktop']/div/div[1]/div[1]/select/option[7]")).click();
		
		driver.findElement(By.xpath(".//*[@id='wci_searchPage_controls_desktop']/div/div[1]/div[1]/select/option[3]")).click();
		
		driver.findElement(By.xpath(".//*[@id='wci_searchPage_controls_desktop']/div/div[1]/div[1]/select/option[5]")).click();
		
		driver.findElement(By.xpath(".//*[@id='contentImage_1_HeaderStoreLogo_Content']")).click();
		 
	 }
	
	
	
	@BeforeClass
	public void OpenBrowser(){
		driver=Utility.openBrowser();
		
	}
	@AfterClass
	public void CloseBrowser(){
		
		driver.quit();
	}

}
