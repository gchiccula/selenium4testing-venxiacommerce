package com.webcorp.ca.TestCases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.webcorp.ca.Helper.Utility;

public class Test07 {
public WebDriver driver;
	
	@Test(description="Verify The Functionality of Forgot Password")
	 public void forgotPassword() throws Exception{
		driver.findElement(By.xpath(".//*[@id='userRegisterButton']")).click();
		
		driver.findElement(By.xpath(".//*[@id='logonId']")).sendKeys("venkata.chakka@webcorpinc.ca");
		
		driver.findElement(By.xpath(".//*[@id='password']")).sendKeys("00-18596");
		
		driver.findElement(By.xpath(".//*[@id='Header_GlobalLogin_WC_AccountDisplay_links_1']")).click();
		 Thread.sleep(3000);
		driver.findElement(By.xpath(".//*[@id='forgotLogonId']")).sendKeys("venkata.chakka@webcorpinc.ca");
		Thread.sleep(3000);
		driver.findElement(By.xpath(".//*[@id='WC_UserRegistrationAddForm_links_1']/div[2]")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='contentImage_1_HeaderStoreLogo_Content']")).click();
	 }
	
	
	
	@BeforeClass
	public void OpenBrowser(){
		driver=Utility.openBrowser();
		
	}
	@AfterClass
	public void CloseBrowser(){
		
		driver.quit();
	}

}
