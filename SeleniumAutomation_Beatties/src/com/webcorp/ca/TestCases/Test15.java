package com.webcorp.ca.TestCases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.webcorp.ca.Helper.Utility;

public class Test15 {
	
public WebDriver driver;
	
	@Test(description="Click On SearchBar")
	 public void SearchBar() throws Exception{
		driver.findElement(By.xpath(".//*[@id='autocomplete']")).click();
		
		driver.findElement(By.xpath(".//*[@id='autocomplete']")).sendKeys("paper");
		
		Thread.sleep(4000);
		
		driver.findElement(By.xpath(".//*[@id='wci_searchBox']/div/div/span/a/img")).click();
		
		driver.findElement(By.xpath(".//*[@id='contentImage_1_HeaderStoreLogo_Content']")).click();
		
		driver.findElement(By.xpath(".//*[@id='autocomplete']")).click();
		
		driver.findElement(By.xpath(".//*[@id='autocomplete']")).sendKeys("pen");
		
		Thread.sleep(4000);
		
		driver.findElement(By.xpath(".//*[@id='wci_searchBox']/div/div/span/a/img")).click();
		
		driver.findElement(By.xpath(".//*[@id='contentImage_1_HeaderStoreLogo_Content']")).click();
		
		 
	 }
	
	
	
	@BeforeClass
	public void OpenBrowser(){
		driver=Utility.openBrowser();
		
	}
	@AfterClass
	public void CloseBrowser(){
		
		driver.quit();
	}

}
