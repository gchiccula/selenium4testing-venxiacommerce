package com.webcorp.ca.TestCases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.webcorp.ca.Helper.Utility;

public class Test14 {
	
public WebDriver driver;
	
	@Test(description="Verify The functionality of Pagination in SearchResultspage")
	 public void Pagination() throws Exception{
		driver.findElement(By.xpath(".//*[@id='BrowseByDepart']")).click();
		
		driver.findElement(By.xpath(".//*[@id='shopByDepartmentList']/li[9]/a")).click();
		
		Thread.sleep(4000);
		
		driver.findElement(By.xpath(".//*[@id='wci_searchPage_controls_desktop']/div/div[2]/a[5]")).click();
		Thread.sleep(4000);
		driver.findElement(By.xpath(".//*[@id='addtoCart_38081']/a/span/img")).click();
		Thread.sleep(4000);
		driver.findElement(By.xpath(".//*[@id='wci_searchPage_controls_desktop']/div/div[2]/a[2]")).click();
		Thread.sleep(4000);
		//driver.findElement(By.xpath(".//*[@id='contentImage_1_HeaderStoreLogo_Content']")).click();
		
		driver.findElement(By.xpath(".//*[@id='addtoCart_25305']/a/span/img")).click();
		Thread.sleep(4000);
		driver.findElement(By.xpath(".//*[@id='wci_searchPage_controls_desktop']/div/div[1]/div[1]/select/option[3]")).click();
		Thread.sleep(4000);
		driver.findElement(By.xpath(".//*[@id='wci_searchPage_controls_desktop']/div/div[1]/div[1]/select/option[5]")).click();
		Thread.sleep(4000);
		driver.findElement(By.xpath(".//*[@id='contentImage_1_HeaderStoreLogo_Content']")).click();
		 
	 }
	
	
	
	@BeforeClass
	public void OpenBrowser(){
		driver=Utility.openBrowser();
		
	}
	@AfterClass
	public void CloseBrowser(){
		
		driver.quit();
	}

}
