package com.webcorp.ca.TestCases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.webcorp.ca.Helper.Utility;
import com.webcorp.ca.pages.LoginPage;

public class Test25 {
	
	public WebDriver driver;
	@Test (description="verify the functionality of  favourite details page by adding a sku ")
	public void FavouriteDetails() throws Exception{
		
		Thread.sleep(5000);
		Actions a=new Actions(driver);
		//WebElement account=driver.findElement(By.xpath(".//*[@id='myaccountButton']"));
		//a.moveToElement(account).build().perform();
		driver.findElement(By.xpath(".//*[@id='myaccountButton']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath(".//*[@id='render_myaccount']/li[1]/a")).click();
        Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='myfav']/ul/li[1]/a")).click();
		Thread.sleep(5000);
		
		/*WebElement str=driver.findElement(By.linkText("Testing Favourite"));
		a.moveToElement(str).build().perform();*/
		try {
			driver.findElement(By.linkText("Testing Favourite1")).click();
			Thread.sleep(5000);
		} catch (Exception e) {
			driver.findElement(By.xpath(".//*[@id='myaccountDropdownButton']")).click();
			Thread.sleep(2000);
			WebElement str=driver.findElement(By.linkText("Testing Favourite1"));
			driver.findElement(By.linkText("Testing Favourite1")).click();
			
		}
		
		
		try {
			driver.findElement(By.xpath(".//*[@id='favdetails_checkbox']")).click();
	        Thread.sleep(3000);
	        driver.findElement(By.xpath(".//*[@id='wci_box']/div[3]/div[3]/div[1]/a")).click();
	        
	        String str4;
			
			str4=driver.switchTo().alert().getText();
			System.out.println(str4);
			driver.switchTo().alert().accept();
			
			Thread.sleep(5000);
		} catch (Exception e) {
			System.out.println("no favourites available");
		}
		
		
		Thread.sleep(5000);
		try {
			driver.findElement(By.linkText("Testing Favourite1")).click();
			
		} catch (Exception e) {
			driver.findElement(By.xpath(".//*[@id='favsku']")).sendKeys("38584-00");
			driver.findElement(By.xpath(".//*[@id='savedcarts']/div[1]/div[2]/div[1]/div[3]/a")).click();
			Thread.sleep(8000);
			driver.findElement(By.xpath(".//*[@id='favsku']")).clear();
			driver.findElement(By.xpath(".//*[@id='favsku']")).sendKeys("38866-00");
			
			driver.findElement(By.xpath(".//*[@id='savedcarts']/div[1]/div[2]/div[1]/div[3]/a")).click();
			Thread.sleep(8000);
			driver.findElement(By.xpath(".//*[@id='favsku']")).clear();
			driver.findElement(By.xpath(".//*[@id='favsku']")).sendKeys("28124-01");
			
			driver.findElement(By.xpath(".//*[@id='savedcarts']/div[1]/div[2]/div[1]/div[3]/a")).click();
			Thread.sleep(8000);
			driver.findElement(By.xpath(".//*[@id='favsku']")).clear();
			driver.findElement(By.xpath(".//*[@id='favsku']")).sendKeys("27013-05");
			
			driver.findElement(By.xpath(".//*[@id='savedcarts']/div[1]/div[2]/div[1]/div[3]/a")).click();
			Thread.sleep(8000);
			driver.findElement(By.xpath(".//*[@id='favdetails_checkbox']")).click();
			//driver.findElement(By.xpath(".//*[@id='favdetails_checkbox']")).click();
	        Thread.sleep(5000);
	        
	        driver.findElement(By.xpath(".//*[@id='wci_box']/div[3]/div[3]/div[2]/a[2]")).click();
	        Thread.sleep(5000);
	        driver.findElement(By.xpath(".//*[@id='widget_minishopcart']")).click();
	        Thread.sleep(5000);
	        driver.findElement(By.xpath(".//*[@id='GotoCartButton1']/span")).click();
	        Thread.sleep(5000); 
	        //driver.findElement(By.xpath(".//*[@id='field3']")).click();
	        driver.findElement(By.xpath(".//*[@id='field3']")).sendKeys("4444");
	        Thread.sleep(3000);
	        driver.findElement(By.xpath(".//*[@id='submitorderbutton']")).click();
	        Thread.sleep(8000);
	        driver.findElement(By.xpath(".//*[@id='render_favouritename']")).click();
	        Thread.sleep(3000);
	        
	        WebElement str2=driver.findElement(By.linkText("Testing Favourite1"));
	       
			a.moveToElement(str2).build().perform();
			driver.findElement(By.linkText("Testing Favourite1")).click();
	        Thread.sleep(3000);
	        driver.findElement(By.xpath(".//*[@id='favdetails_checkbox']")).click();
	        Thread.sleep(3000);
	        driver.findElement(By.xpath(".//*[@id='wci_box']/div[3]/div[3]/div[1]/a")).click();
	        
	        String str3;
			
			str3=driver.switchTo().alert().getText();
			System.out.println(str3);
			driver.switchTo().alert().accept();
			
			Thread.sleep(5000);
			
			driver.findElement(By.xpath(".//*[@id='contentImage_1_HeaderStoreLogo_Content']")).click();
			Thread.sleep(3000);
		}
		
		
	
	}
	
	@BeforeClass
	public void OpenBrowser() throws Exception{
	
		driver=Utility.openBrowser();
			Thread.sleep(5000);
		
		
		}

@BeforeClass
public void SignIn() throws Exception{
	
    LoginPage login=PageFactory.initElements(driver, LoginPage.class);
	
	login.SignIn();
	Thread.sleep(5000);
}
@AfterClass
public void LogOut() throws Exception{
	driver.findElement(By.xpath(".//*[@id='myaccountButton']")).click();
	Thread.sleep(3000);
	driver.findElement(By.xpath(".//*[@id='logOut']")).click();
	Thread.sleep(3000);
	driver.quit();
	
}

}
