package com.webcorp.ca.TestCases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.webcorp.ca.Helper.Utility;

public class Test13 {
	
public WebDriver driver;
	
	
	@Test(description="verify the functionality of Filter By In Search Results Page")
	public void FilterBy() throws Exception{
		
		driver.findElement(By.xpath(".//*[@id='BrowseByDepart']")).click();
		
		driver.findElement(By.xpath(".//*[@id='shopByDepartmentList']/li[1]/a")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='category']/ul/li[3]/a")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='wci_widget_breadcrumb']/ul/li[1]/a")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='BrowseByDepart']")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='shopByDepartmentList']/li[9]/a")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='category']/ul/li[6]/a")).click();
		Thread.sleep(4000);
		driver.findElement(By.xpath(".//*[@id='wci_widget_breadcrumb']/ul/li[1]/a")).click();
		
		
		
		
		
	}
	
	@BeforeClass
	public void OpenBrowser(){
		driver=Utility.openBrowser();
		
	}
	@AfterClass
	public void CloseBrowser(){
		
		driver.quit();
	}

}
