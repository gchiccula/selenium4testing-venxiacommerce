package com.webcorp.ca.TestCases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.webcorp.ca.Helper.Utility;
import com.webcorp.ca.pages.BillingAddressPage;
import com.webcorp.ca.pages.PaymentPage;

public class Test11 {
	
public WebDriver driver;
	
	@Test(description="Verify The Functionality Of Checkout Process Search Results Page")
	 public void SubmitOrderAbove20Items() throws Exception{
		
        driver.findElement(By.xpath(".//*[@id='BrowseByDepart']")).click();
        Thread.sleep(4000);
		driver.findElement(By.xpath(".//*[@id='shopByDepartmentList']/li[1]/a")).click();
		Thread.sleep(4000);
		driver.findElement(By.xpath(".//*[@id='addtoCart_51018']/a/span/img")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='addtoCart_43116']/a/span/img")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='addtoCart_55227']/a/span/img")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='addtoCart_13931']/a/span/img")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='addtoCart_35521']/a/span/img")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='addtoCart_40024']/a/span/img")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='addtoCart_24079']/a/span/img")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='addtoCart_24108']/a/span/img")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='addtoCart_31996']/a/span/img")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='addtoCart_23007']/a/span/img")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='addtoCart_70036']/a/span/img")).click();
		Thread.sleep(4000);
		driver.findElement(By.xpath(".//*[@id='contentImage_1_HeaderStoreLogo_Content']")).click();
		Thread.sleep(4000);
		driver.findElement(By.xpath(".//*[@id='BrowseByDepart']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='shopByDepartmentList']/li[7]/a")).click();
		Thread.sleep(4000);
		driver.findElement(By.xpath(".//*[@id='addtoCart_43116']/a/span/img")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='addtoCart_02015']/a/span/img")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='addtoCart_59073']/a/span/img")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='addtoCart_02067']/a/span/img")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='addtoCart_02029']/a/span/img")).click();
		Thread.sleep(1000);
		//driver.findElement(By.xpath(".//*[@id='addtoCart_10153']/a/span/img")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='addtoCart_02035']/a/span/img")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='addtoCart_02216']/a/span/img")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='addtoCart_02162']/a/span/img")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='addtoCart_02255']/a/span/img")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='addtoCart_02265']/a/span/img")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='addtoCart_59051']/a/span/img")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='addtoCart_02285']/a/span/img")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='addtoCart_63037']/a/span/img")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='addtoCart_04022']/a/span/img")).click();
		//Thread.sleep(4000);
		/*driver.findElement(By.xpath(".//*[@id='wci_searchPage_controls_desktop']/div/div[2]/a[3]")).click();
		Thread.sleep(4000);
		driver.findElement(By.xpath(".//*[@id='addtoCart_10011']/a/span/img")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='addtoCart_10026']/a/span/img")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='addtoCart_10008']/a/span/img")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='addtoCart_10013']/a/span/img")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='addtoCart_10064']/a/span/img")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='addtoCart_10066']/a/span/img")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='addtoCart_10059']/a/span/img")).click();*/
		Thread.sleep(5000);
		/*driver.findElement(By.xpath(".//*[@id='widget_minishopcart']")).click();
		Thread.sleep(4000);*/
		driver.findElement(By.xpath(".//*[@id='widget_minishopcart']")).click();
		Thread.sleep(8000);
		/*driver.findElement(By.xpath(".//*[@id='GotoCartButton1']/span")).click();
		Thread.sleep(6000);*/
		driver.findElement(By.xpath(".//*[@id='submitorderbutton']")).click();
		Thread.sleep(4000);
         BillingAddressPage billing=PageFactory.initElements(driver, BillingAddressPage.class);
		
		billing.FillBillingAddressForm();
		Thread.sleep(4000);
		driver.findElement(By.xpath(".//*[@id='WC_UserRegistrationAddForm_links_1']/div[2]")).click();
		Thread.sleep(4000);
		driver.findElement(By.xpath(".//*[@id='sameAsBilling']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath(".//*[@id='WC_UserRegistrationAddForm_links_1']/div[2]")).click();
		Thread.sleep(4000);
		PaymentPage payment=PageFactory.initElements(driver, PaymentPage.class);
		payment.paymentPageForm();
		Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='submitorderbutton']")).click();
		try {
			driver.switchTo().alert().accept();
			
		} catch (Exception e) {
			System.out.println("shipping charges are not added");
		}
		Thread.sleep(10000);
		//driver.findElement(By.xpath("html/body/div[2]/div/div[1]/div/div[2]/div[2]/p[4]/span/a/button")).click();
		 
	 }
	
	
	
	@BeforeClass
	public void OpenBrowser(){
		driver=Utility.openBrowser();
		
	}
	@AfterClass
	public void CloseBrowser(){
		
		driver.quit();
	}

}
