package com.webcorp.ca.TestCases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.webcorp.ca.Helper.Utility;
import com.webcorp.ca.pages.LoginPage;

public class Test09 {
	
	public WebDriver driver;
	@Test (description="verify the functionality of  Changing Attribute color For Item")
	public void ChangingAttributeColor() throws Exception{
		Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='BrowseByDepart']")).click();
		
		driver.findElement(By.xpath(".//*[@id='shopByDepartmentList']/li[13]/a")).click();
		Thread.sleep(5000);
		new Select(driver.findElement(By.xpath(".//*[@id='sku_10814_listImage']"))).selectByVisibleText("Magenta");
		Thread.sleep(5000);
		try {
		driver.findElement(By.xpath(".//*[@id='addtofavcartbutton_10814']/a/span/img")).click();
		Thread.sleep(5000);
			
		} catch (Exception e) {
			driver.findElement(By.xpath(".//*[@id='listViewAdd2Cart_3074457345616679832']/img")).click();
		}
		new Select(driver.findElement(By.xpath(".//*[@id='sku_10814_listImage']"))).selectByVisibleText("Magenta");
		Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='addtofavcartbutton_10814']/a/span/img")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='widget_minishopcart']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath(".//*[@id='GotoCartButton1']")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='field3']")).sendKeys("4444");
        Thread.sleep(3000);
		driver.findElement(By.xpath(".//*[@id='submitorderbutton']")).click();
		try {
			driver.switchTo().alert().accept();
			
		} catch (Exception e) {
			System.out.println("shipping charges are not added");
		}
		Thread.sleep(7000);
		//driver.findElement(By.xpath("html/body/div[2]/div/div[1]/div/div[2]/div[2]/p[5]/span/a/button")).click();
		
	}
	
	@BeforeClass
	public void OpenBrowser() throws Exception{
	
		driver=Utility.openBrowser();
			Thread.sleep(5000);
		
		
		}

@BeforeClass
public void SignIn() throws Exception{
	
LoginPage login=PageFactory.initElements(driver, LoginPage.class);
	
	login.SignIn();
	Thread.sleep(5000);
}
@AfterClass
public void LogOut() throws Exception{
	driver.findElement(By.xpath(".//*[@id='myaccountButton']")).click();
	Thread.sleep(3000);
	driver.findElement(By.xpath(".//*[@id='logOut']")).click();
	Thread.sleep(3000);
	driver.quit();
	
}

}
