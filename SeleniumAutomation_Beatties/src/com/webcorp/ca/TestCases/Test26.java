package com.webcorp.ca.TestCases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.webcorp.ca.Helper.Utility;
import com.webcorp.ca.pages.LoginPage;

public class Test26 {
	
	public WebDriver driver;
	@Test (description="verify the functionality of  my favourites list ")
	public void FavouritesList() throws Exception{
		Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='myaccountButton']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath(".//*[@id='render_myaccount']/li[1]/a")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='myfav']/ul/li[1]/a")).click();
		Thread.sleep(5000);
       /* driver.findElement(By.xpath(".//*[@id='MyFav_DataTable']/tbody/tr[2]/td[1]/span/a")).click();
        Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='myfav']/ul/li[1]/a")).click();
		Thread.sleep(5000);*/
		/*Actions a=new Actions(driver);
		WebElement str=driver.findElement(By.linkText("Testing Favourite"));
		a.moveToElement(str).build().perform();
		driver.findElement(By.xpath(".//*[@id='MyFav_DataTable']/tbody/tr[2]/td[1]/span/a")).click();
		Thread.sleep(5000);*/
		
	
	}
	
	@BeforeClass
	public void OpenBrowser() throws Exception{
	
		driver=Utility.openBrowser();
			Thread.sleep(5000);
		
		
		}

@BeforeClass
public void SignIn() throws Exception{
	
    LoginPage login=PageFactory.initElements(driver, LoginPage.class);
	
	login.SignIn();
	Thread.sleep(5000);
}
@AfterClass
public void LogOut() throws Exception{
	driver.findElement(By.xpath(".//*[@id='myaccountButton']")).click();
	Thread.sleep(3000);
	driver.findElement(By.xpath(".//*[@id='logOut']")).click();
	Thread.sleep(3000);
	driver.quit();
	
}

}
