package com.webcorp.ca.TestCases;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.webcorp.ca.Helper.Utility;
import com.webcorp.ca.pages.LoginPage;

public class Test21 {
	
	public WebDriver driver;
	@Test (description="verify the functionality of  submit order in QuickOrderForm with register user")
	public void SubmitOrderForQuickOrderFormForRegister() throws Exception{
		Thread.sleep(5000);
		    driver.findElement(By.xpath(".//*[@id='myAccountQuickLink']")).click();
			Thread.sleep(4000);
	       driver.findElement(By.xpath(".//*[@id='itemNumber_1']")).sendKeys("51018-00");
			
			Thread.sleep(4000);
			driver.findElement(By.xpath(".//*[@id='itemNumber_2']")).sendKeys("");
			Thread.sleep(4000);
			driver.findElement(By.xpath(".//*[@id='itemNumber_2']")).sendKeys("46219-00");
			Thread.sleep(4000);
			driver.findElement(By.xpath(".//*[@id='itemNumber_3']")).sendKeys("");
			Thread.sleep(4000);
			JavascriptExecutor jse = (JavascriptExecutor)driver; 
			jse.executeScript("window.scrollBy(0,350)", ""); 
			Thread.sleep(3000);
			driver.findElement(By.xpath(".//*[@id='addtoCart']/img")).click();
			Thread.sleep(12000);
			/*Actions a=new Actions(driver);
			WebElement str=driver.findElement(By.xpath(".//*[@id='widget_minishopcart']"));
			a.moveToElement(str).build().perform();*/
			driver.findElement(By.xpath(".//*[@id='widget_minishopcart']")).click();
			Thread.sleep(5000);
		    driver.findElement(By.xpath(".//*[@id='GotoCartButton1']")).click();
		    Thread.sleep(5000);
		    //driver.findElement(By.xpath(".//*[@id='field3']")).click();
		    driver.findElement(By.xpath(".//*[@id='field3']")).sendKeys("4444");
	        Thread.sleep(3000);
		   driver.findElement(By.xpath(".//*[@id='submitorderbutton']")).click();
		   try {
				driver.switchTo().alert().accept();
				
			} catch (Exception e) {
				System.out.println("shipping charges are not added");
			}
		 /* Thread.sleep(5000);
          driver.findElement(By.xpath(".//*[@id='contentImage_1_HeaderStoreLogo_Content']")).click();*/
		   Thread.sleep(8000);
		
	}
	
	@BeforeClass
	public void OpenBrowser() throws Exception{
	
		driver=Utility.openBrowser();
			Thread.sleep(5000);
		
		
		}

@BeforeClass
public void SignIn() throws Exception{
	
    LoginPage login=PageFactory.initElements(driver, LoginPage.class);
	
	login.SignIn();
	Thread.sleep(5000);
}
@AfterClass
public void LogOut() throws Exception{
	driver.findElement(By.xpath(".//*[@id='myaccountButton']")).click();
	Thread.sleep(3000);
	driver.findElement(By.xpath(".//*[@id='logOut']")).click();
	Thread.sleep(3000);
	driver.quit();
	
}


}
