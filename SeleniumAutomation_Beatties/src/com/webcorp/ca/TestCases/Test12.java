package com.webcorp.ca.TestCases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.webcorp.ca.Helper.Utility;

public class Test12 {
	
public WebDriver driver;
	
	@Test(description="Click All Categories")
	 public void ClickOnBrowseByDepartmentOneByOne(){
		driver.findElement(By.xpath(".//*[@id='BrowseByDepart']")).click();
		
		driver.findElement(By.xpath(".//*[@id='shopByDepartmentList']/li[1]/a")).click();
		
		driver.findElement(By.xpath(".//*[@id='contentImage_1_HeaderStoreLogo_Content']")).click();
		driver.findElement(By.xpath(".//*[@id='BrowseByDepart']")).click();
		driver.findElement(By.xpath(".//*[@id='shopByDepartmentList']/li[3]/a")).click();
		
        driver.findElement(By.xpath(".//*[@id='contentImage_1_HeaderStoreLogo_Content']")).click();
        driver.findElement(By.xpath(".//*[@id='BrowseByDepart']")).click();
		driver.findElement(By.xpath(".//*[@id='shopByDepartmentList']/li[5]/a")).click();
		
        driver.findElement(By.xpath(".//*[@id='contentImage_1_HeaderStoreLogo_Content']")).click();
        driver.findElement(By.xpath(".//*[@id='BrowseByDepart']")).click();
		driver.findElement(By.xpath(".//*[@id='shopByDepartmentList']/li[7]/a")).click();
		
        driver.findElement(By.xpath(".//*[@id='contentImage_1_HeaderStoreLogo_Content']")).click();
        driver.findElement(By.xpath(".//*[@id='BrowseByDepart']")).click();
		driver.findElement(By.xpath(".//*[@id='shopByDepartmentList']/li[9]/a")).click();
		
        driver.findElement(By.xpath(".//*[@id='contentImage_1_HeaderStoreLogo_Content']")).click();
        driver.findElement(By.xpath(".//*[@id='BrowseByDepart']")).click();
		driver.findElement(By.xpath(".//*[@id='shopByDepartmentList']/li[11]/a")).click();
		
        driver.findElement(By.xpath(".//*[@id='contentImage_1_HeaderStoreLogo_Content']")).click();
        driver.findElement(By.xpath(".//*[@id='BrowseByDepart']")).click();
		driver.findElement(By.xpath(".//*[@id='shopByDepartmentList']/li[13]/a")).click();
		
        driver.findElement(By.xpath(".//*[@id='contentImage_1_HeaderStoreLogo_Content']")).click();
        driver.findElement(By.xpath(".//*[@id='BrowseByDepart']")).click();
		driver.findElement(By.xpath(".//*[@id='shopByDepartmentList']/li[15]/a")).click();
		
        driver.findElement(By.xpath(".//*[@id='contentImage_1_HeaderStoreLogo_Content']")).click();
        driver.findElement(By.xpath(".//*[@id='BrowseByDepart']")).click();
		driver.findElement(By.xpath(".//*[@id='shopByDepartmentList']/li[17]/a")).click();
		
        driver.findElement(By.xpath(".//*[@id='contentImage_1_HeaderStoreLogo_Content']")).click();
        driver.findElement(By.xpath(".//*[@id='BrowseByDepart']")).click();
		driver.findElement(By.xpath(".//*[@id='shopByDepartmentList']/li[19]/a")).click();
		
		driver.findElement(By.xpath(".//*[@id='contentImage_1_HeaderStoreLogo_Content']")).click();
		 
	 }
	
	
	
	@BeforeClass
	public void OpenBrowser(){
		driver=Utility.openBrowser();
		
	}
	@AfterClass
	public void CloseBrowser(){
		
		driver.quit();
	}

}
