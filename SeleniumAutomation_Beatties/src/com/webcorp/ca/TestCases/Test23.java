package com.webcorp.ca.TestCases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.webcorp.ca.Helper.Utility;
import com.webcorp.ca.pages.LoginPage;

public class Test23 {
	
	public WebDriver driver;
	@Test (description="verify the functionality of  submit order in ProductDetails Page with register user")
	public void PdpSubmitOrderForRegister() throws Exception{
		Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='BrowseByDepart']")).click();
		
		driver.findElement(By.xpath(".//*[@id='shopByDepartmentList']/li[9]/a")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='product_search_name_25407']")).click();
		Thread.sleep(8000);
		driver.findElement(By.xpath(".//*[@id='ProductListDataTableId_25407-46']/td[5]/span/img")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='widget_minishopcart']")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='GotoCartButton1']")).click();
		Thread.sleep(8000);
		//driver.findElement(By.xpath(".//*[@id='field3']")).click();
		driver.findElement(By.xpath(".//*[@id='field3']")).sendKeys("4444");
        Thread.sleep(3000);
		driver.findElement(By.xpath(".//*[@id='submitorderbutton']")).click();
		
		try {
			driver.switchTo().alert().accept();
			
		} catch (Exception e) {
			System.out.println("shipping charges are not added");
		}
		Thread.sleep(8000);
       // driver.findElement(By.xpath("html/body/div[2]/div/div[1]/div/div[2]/div[2]/p[5]/span/a/button")).click();
		
	}
	
	@BeforeClass
	public void OpenBrowser() throws Exception{
	
		driver=Utility.openBrowser();
			Thread.sleep(5000);
		
		
		}

@BeforeClass
public void SignIn() throws Exception{
	
    LoginPage login=PageFactory.initElements(driver, LoginPage.class);
	
	login.SignIn();
	Thread.sleep(5000);
}
@AfterClass
public void LogOut() throws Exception{
	driver.findElement(By.xpath(".//*[@id='myaccountButton']")).click();
	Thread.sleep(3000);
	driver.findElement(By.xpath(".//*[@id='logOut']")).click();
	Thread.sleep(3000);
	driver.quit();
	
}

}
