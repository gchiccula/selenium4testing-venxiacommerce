package com.webcorp.ca.TestCases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.webcorp.ca.Helper.Utility;
import com.webcorp.ca.pages.BillingAddressPage;
import com.webcorp.ca.pages.PaymentPage;

public class Test03 {

public WebDriver driver;
	
	@Test(description="Checkout Homepage")
	 public void homepageSldierUpAds() throws Exception{
			driver.findElement(By.xpath(".//*[@id='slide_slideup']/div/span[2]/a/img")).click();
	      Thread.sleep(5000);
		
		driver.findElement(By.xpath(".//*[@id='widget_minishopcart']")).click();
		/*Thread.sleep(2000);
		driver.findElement(By.xpath(".//*[@id='widget_minishopcart']")).click();*/
		Thread.sleep(2000);
		//driver.findElement(By.xpath(".//*[@id='GotoCartButton1']/span")).click();
		Thread.sleep(4000);
		driver.findElement(By.xpath(".//*[@id='submitorderbutton']")).click();
		Thread.sleep(4000);
        BillingAddressPage billing=PageFactory.initElements(driver, BillingAddressPage.class);
		
		billing.FillBillingAddressForm();
		
		driver.findElement(By.xpath(".//*[@id='WC_UserRegistrationAddForm_links_1']/div[2]")).click();
		Thread.sleep(4000);
		driver.findElement(By.xpath(".//*[@id='sameAsBilling']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath(".//*[@id='WC_UserRegistrationAddForm_links_1']/div[2]")).click();
		Thread.sleep(4000);
		PaymentPage payment=PageFactory.initElements(driver, PaymentPage.class);
		payment.paymentPageForm();
		
		
		
		driver.findElement(By.xpath(".//*[@id='submitorderbutton']")).click();
		try {
			driver.switchTo().alert().accept();
			
		} catch (Exception e) {
			System.out.println("shipping charges are not added");
		}
		Thread.sleep(6000);
		driver.findElement(By.xpath("html/body/div[2]/div/div[1]/div/div[2]/div[2]/p[4]/span/a/button")).click();
		 
	 }
	
	
	
	@BeforeClass
	public void OpenBrowser(){
		driver=Utility.openBrowser();
		
	}
	@AfterClass
	public void CloseBrowser(){
		
		driver.quit();
	}

}
