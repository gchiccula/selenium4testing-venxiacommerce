package com.webcorp.ca.TestCases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.webcorp.ca.Helper.Utility;
import com.webcorp.ca.pages.LoginPage;

public class Test24 {
	
public WebDriver driver;
	
	@Test (description="verify the functionality of  Add to favourite ")
	public void Favourites() throws Exception{
		Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='myaccountDropdownButton']")).click();
		Thread.sleep(2000);
		Actions a=new Actions(driver);
		WebElement str=driver.findElement(By.linkText("Testing Favourite1"));
		driver.findElement(By.linkText("Testing Favourite1")).click();
		driver.findElement(By.xpath(".//*[@id='contentImage_1_HeaderStoreLogo_Content']")).click();
		Thread.sleep(5000);
		/*driver.findElement(By.xpath(".//*[@id='render_favouritename']")).click();
		Thread.sleep(5000);
        driver.findElement(By.xpath(".//*[@id='render_favouritesDropDown']/li[1]/a")).click();
        Thread.sleep(5000);*/
		driver.findElement(By.xpath(".//*[@id='BrowseByDepart']")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='shopByDepartmentList']/li[5]/a")).click();
		/*Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='addtofav_28029']/a/span/img")).click();*/
		Thread.sleep(6000);
		driver.findElement(By.xpath(".//*[@id='addtofav_24108']/a/span/img")).click();
		Thread.sleep(8000);
		driver.findElement(By.xpath(".//*[@id='addtofav_23007']/a/span/img")).click();
		Thread.sleep(6000);
		driver.findElement(By.xpath(".//*[@id='addtofav_51420']/a/span/img")).click();
		Thread.sleep(6000);
		driver.findElement(By.xpath(".//*[@id='addtofav_51421']/a/span/img")).click();
		Thread.sleep(6000);
		driver.findElement(By.xpath(".//*[@id='addtofav_51047']/a/span/img")).click();
		Thread.sleep(6000);
		driver.findElement(By.xpath(".//*[@id='render_favouritename']")).click();
		Thread.sleep(2000);
		Actions a1=new Actions(driver);
		WebElement str1=driver.findElement(By.linkText("Testing Favourite1"));
		//a1.moveToElement(str1).build().perform();
		driver.findElement(By.linkText("Testing Favourite1")).click();
		//driver.findElement(By.linkText("Testing Favourite")).click();
		Thread.sleep(5000);
        driver.findElement(By.xpath(".//*[@id='favdetails_checkbox']")).click();
        Thread.sleep(5000);
        driver.findElement(By.xpath(".//*[@id='wci_box']/div[3]/div[3]/div[2]/a[2]")).click();
        Thread.sleep(5000);
        driver.findElement(By.xpath(".//*[@id='widget_minishopcart']")).click();
        Thread.sleep(5000);
        driver.findElement(By.xpath(".//*[@id='GotoCartButton1']/span")).click();
        Thread.sleep(5000);
       // driver.findElement(By.xpath(".//*[@id='field3']")).click();
        driver.findElement(By.xpath(".//*[@id='field3']")).sendKeys("4444");
        Thread.sleep(3000);
        driver.findElement(By.xpath(".//*[@id='submitorderbutton']")).click();
        Thread.sleep(10000);
        driver.findElement(By.xpath(".//*[@id='myaccountDropdownButton']")).click();
        Thread.sleep(3000);
        WebElement str2=driver.findElement(By.linkText("Testing Favourite1"));
		a.moveToElement(str2).build().perform();
		driver.findElement(By.linkText("Testing Favourite1")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath(".//*[@id='favdetails_checkbox']")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath(".//*[@id='wci_box']/div[3]/div[3]/div[1]/a")).click();
        
        String str3;
		
		str3=driver.switchTo().alert().getText();
		System.out.println(str3);
		driver.switchTo().alert().accept();
		
		
		Thread.sleep(5000);
		
		driver.findElement(By.xpath(".//*[@id='contentImage_1_HeaderStoreLogo_Content']")).click();
		Thread.sleep(3000);
		
	}
	
	@BeforeClass
	public void OpenBrowser() throws Exception{
	
		driver=Utility.openBrowser();
			Thread.sleep(5000);
		
		
		}

@BeforeClass
public void SignIn() throws Exception{
	
    LoginPage login=PageFactory.initElements(driver, LoginPage.class);
	
	login.SignIn();
	Thread.sleep(5000);
}
@AfterClass
public void LogOut() throws Exception{
	driver.findElement(By.xpath(".//*[@id='myaccountButton']")).click();
	Thread.sleep(3000);
	driver.findElement(By.xpath(".//*[@id='logOut']")).click();
	Thread.sleep(3000);
	driver.quit();
	
}

}
